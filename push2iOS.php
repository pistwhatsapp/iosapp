<?php
header("Content-Type: text/html; charset=utf-8");

if($_POST['message']){	
	// Put your device token here (without spaces):
	$deviceToken = '';

	// Put your private key's passphrase here:
	$passphrase = 'emnemn';

	// Put your alert message here:
	$message = stripslashes($_POST['message']);
	echo $message . '<br />' . PHP_EOL;

	////////////////////////////////////////////////////////////////////////////////

	$ctx = stream_context_create();
	stream_context_set_option($ctx, 'ssl', 'local_cert', 'PistachePushCK.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

	// Open a connection to the APNS server
	$fp = stream_socket_client(
		'ssl://gateway.sandbox.push.apple.com:2195', $err,
		$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

	if (!$fp)
		exit("Failed to connect: $err $errstr <br />" . PHP_EOL);

	echo 'Connected to APNS<br />' . PHP_EOL;

	// Create the payload body
	$body['aps'] = array(
		'alert' => $message,
		'sound' => 'default'
		);

	// Encode the payload as JSON
	$payload = json_encode($body);

	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));

	if (!$result)
		echo 'Message not delivered<br />' . PHP_EOL;
	else
		echo 'Message successfully delivered<br />' . PHP_EOL;

	// Close the connection to the server
	fclose($fp);
}
?>

<form action="push2iOS.php" method="post">
	<input type="text" name="message" maxlength="100" />
	<input type="submit" value="Send Notification" />
</form>