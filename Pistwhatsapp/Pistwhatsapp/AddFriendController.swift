//
//  AddFriendController.swift
//  Pistwhatsapp
//
//  Created by Thomas Garesse on 03/04/2015.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AddFriendController: UITableViewController {

    // MARK:- Model
    var users: [(email: String, selected: Bool)] = []
    var currentFriends = [String]()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        return (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!
    }()
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as AppDelegate
    }()
    
    var delegate: AddFriendControllerDelegate?
    
    // MARK:- View life cycle
    override func viewDidLoad() {
        self.currentFriends = self.delegate!.currentFriends()
        getAllUsers()
    }
    
    // MARK:- UI methods
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func comfirmAddFriends(sender: UIBarButtonItem) {
        addSelectedUsers()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserCell") as UITableViewCell
        cell.textLabel?.text = users[indexPath.row].email
        if users[indexPath.row].selected {
            cell.accessoryType = UITableViewCellAccessoryType.Checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.None
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if cell?.accessoryType == UITableViewCellAccessoryType.Checkmark {
            cell?.accessoryType = UITableViewCellAccessoryType.None
            users[indexPath.row].selected = false
        } else {
            cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
            users[indexPath.row].selected = true
        }
    }
    
    // MARK:- HTTP
    private func getAllUsers() {
        if let url = NSURL(string: "https://pist-minesnantes.rhcloud.com/getusers.php") {
            var request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            
            let usermail = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserEmail")!
            let postString = "user=\(usermail)"
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                (data, response, error) in
                
                println("response = \(response)")
                var parseError: NSError?
                let parsedObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data!,
                    options: NSJSONReadingOptions.AllowFragments,
                    error:&parseError)
                println(data)
                let currentUser = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserEmail")!
                if let userArray = parsedObject as? Array<String> {
                    for mail in userArray {
                        if mail != currentUser && !contains(self.currentFriends, mail){
                            self.users += [(mail, false)]
                        }
                    }
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.tableView.reloadData()
                }
                
            }
            
            task.resume()
        }
    }
    
    private func addSelectedUsers() {
        var selected = [String]()
        for user in users {
            if user.selected {
                selected += [user.email]
            }
        }
        
        if selected.isEmpty {
            return
        }
        
        let currentUser = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserEmail")!
        var body = "user=\(currentUser)"
        for mail in selected {
            body += "&contacts[]=\(mail.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!)"
        }
        
        if let url = NSURL(string: "https://pist-minesnantes.rhcloud.com/addcontacts.php") {
            var request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            
            request.HTTPBody = body.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                (data, response, error) in
                
                var parseError: NSError?
                let parsedObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data!,
                    options: NSJSONReadingOptions.AllowFragments,
                    error:&parseError)
                
                let currentUser = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserEmail")!
                if let userArray = parsedObject as? Array<String> {
                    dispatch_async(dispatch_get_main_queue()) {
                        println(userArray)
                        for friend in userArray {
                            if friend.contains("[") || friend.contains(",") || !contains(selected, friend) {
                                continue
                            }
                            
                            let fetchRequest = NSFetchRequest()
                            let entity = NSEntityDescription.entityForName("User", inManagedObjectContext: self.managedObjectContext)
                            fetchRequest.entity = entity
                            // Specify criteria for filtering which objects to fetch
                            let predicate = NSPredicate(format: "mail==%@", friend)
                            fetchRequest.predicate = predicate
                            
                            if let fetchedObjects = self.managedObjectContext.executeFetchRequest(fetchRequest, error: nil)  {
                                if fetchedObjects.count == 0 {
                                    let friendName = friend.componentsSeparatedByString("@")[0]
                                    User.createInManagedObjectContext(self.managedObjectContext, mail: friend, username: friend, name: friendName, surname: "", regid: "", type: "")
                                }
                            }
                        }
                        self.appDelegate.saveContext()
                        self.delegate?.didAddFriend()
                    }
                }
                
            }
            
            task.resume()
        }

    }

}

protocol AddFriendControllerDelegate {
    func didAddFriend()
    func currentFriends() -> [String]
}

extension String {
    
    func contains(find: String) -> Bool{
        return self.rangeOfString(find) != nil
    }
}
