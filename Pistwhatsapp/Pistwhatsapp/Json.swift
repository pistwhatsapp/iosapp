//
//  Json.swift
//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 14/05/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import Foundation

class Json {
    class func JSONStringify(value: AnyObject, prettyPrinted: Bool = false) -> String {
        var options = prettyPrinted ? NSJSONWritingOptions.PrettyPrinted : nil
        if NSJSONSerialization.isValidJSONObject(value) {
            if let data = NSJSONSerialization.dataWithJSONObject(value, options: options, error: nil) {
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding) {
                    return string
                }
            }
        }
        return ""
    }
}