//
//  LoginViewController.swift
//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 13/02/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class LoginViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var userAccountField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var error: UITextView!
    
    var username = ""
    var errorText = ""
    var probleme = ""
    var connected = false
    let AUTO_LOGIN_TIMEOUT: NSTimeInterval = -60*10   // 10 mins
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as AppDelegate
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        return (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!
    }()
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userAccountField.delegate = self
        self.passwordField.delegate = self
        if let lastLogoutDate = NSUserDefaults.standardUserDefaults().objectForKey("PistacheLastLogoutDate") as? NSDate {
            println("\(lastLogoutDate.timeIntervalSinceNow) s since last logout")
            let isLogin = NSUserDefaults.standardUserDefaults().boolForKey("PistacheIsLogin")
            if  isLogin && lastLogoutDate.timeIntervalSinceNow > AUTO_LOGIN_TIMEOUT {
                if let usermail = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserEmail") {
                    if let usermdp = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserPassword") {
                        if !usermail.isEmpty && !usermdp.isEmpty {
                            println("Auto Login")
                            connected = true
                            nextview()
                        }
                    }
                }
            }
        }
        
        if !connected {
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "PistacheIsLogin")
        }
    }
    
    // MARK: - Actions
    // Change input focus or sumit login request when user press Reture Key
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.userAccountField {
            self.passwordField.becomeFirstResponder()
        } else if textField == self.passwordField {
            textField.resignFirstResponder()
            if !self.userAccountField.text.isEmpty && !self.passwordField.text.isEmpty {
                self.send()
            } else {
                error.textColor=UIColor.redColor()
                error.hidden=false
            }
        }
        return true
    }
    
    @IBAction func login(sender: UIButton) {
        send()
    }
    
    private func nextview(){
        println("nextview")
        if(connected){
            println("connected")
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: "PistacheIsLogin")
            NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: "PistacheLastLogoutDate")
            self.performSegueWithIdentifier("login", sender: self);
        } else {
            NSUserDefaults.standardUserDefaults().setBool(false, forKey: "PistacheIsLogin")

            error.text = "Le serveur n'a pas repondu a temps. \nVeuillez recommencer"
            
            if(probleme == "password"){
                println("nextview-password")
                error.text = "Le mot de passe est incorrect.\nVeuillez recommencer"
                
            } else {
                if(probleme == "user"){
                    println("nextview-user")
                    error.text = "L'identifiant est incorrect.\nVeuillez recommencer"
                }
            }
            println("not_hidden")
            error.textColor=UIColor.redColor()
            error.hidden=false
        }
    }
    
    private func send() {
        if let usermail = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserEmail") {
            if let usermdp = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserPassword") {
                if let username = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserName") {
                    println("usermail: \(usermail), name: \(username), mdp: \(usermdp)")
                    let inputUserAccount = userAccountField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                    userAccountField.text = nil
                    let inputPassword = passwordField.text
                    passwordField.text = nil
                    if(inputUserAccount == usermail && inputPassword == usermdp) {
                        self.connected = true
                    } else {
                        self.connected = false
                        if(inputUserAccount == usermail) {
                            self.probleme="password"
                        } else {
                            self.probleme="user"
                        }
                    }
                }
            }
        }
        
        self.nextview()
    }
}
