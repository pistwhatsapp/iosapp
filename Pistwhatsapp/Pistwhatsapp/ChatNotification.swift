//
//  ChatNotification.swift
//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 13/03/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import Foundation

enum ChatNotification: String {
    case MessageDidReceive = "MessageDidReceived"
    case CurrentFriendMailDidChange = "CurrentFriendMailDidChange"
}
