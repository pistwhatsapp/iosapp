//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 04/02/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import UIKit

class RevealViewController: SWRevealViewController {
    
    override init() {
        super.init()
        config()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        config()
    }
    
    func config() {
        self.rearViewRevealWidth = 250
        self.draggableBorderWidth = 50
        self.toggleAnimationDuration = 0.2
        self.frontViewShadowRadius = 1
        self.frontViewShadowOffset = CGSizeZero
    }
}