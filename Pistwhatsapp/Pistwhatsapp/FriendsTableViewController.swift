//
//  FriendsTableViewController.swift
//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 13/02/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import UIKit
import CoreData

class FriendsTableViewController: UITableViewController, AddFriendControllerDelegate {

    var usernameField: UITextField?
    var titleButton: UIButton?

    var username = ""
    
    // MARK:- Model variables
    lazy var managedObjectContext: NSManagedObjectContext = {
        return (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!
    }()
    
    var friends: [(name: String, mail: String, msg: String)] = []
    
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as AppDelegate
    }()
    
    // MARK:- Functions
    
    // MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let username = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserName"){
            self.username = username
        }
        
        let button =  UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        button.frame = CGRectMake(0, 0, 100, 40) as CGRect
        button.setTitle(username, forState: UIControlState.Normal)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.addTarget(self, action: Selector("clickOnButton:"), forControlEvents: UIControlEvents.TouchUpInside)
        self.navigationItem.titleView = button
        self.titleButton = button
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("messageDidReceive:"), name: ChatNotification.MessageDidReceive.rawValue, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.getFriends()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.tableView.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            switch identifier {
            case "chat" :
                if let indexPath = sender as? NSIndexPath {
                    var mail = self.friends[indexPath.row].mail
                    println("fiends table mail : \(mail)");
                    NSUserDefaults.standardUserDefaults().setObject(mail, forKey: "CurrentFriendMail")
                    NSUserDefaults.standardUserDefaults().setObject(self.friends[indexPath.row].name, forKey: "CurrentFriendName")
                }
            case "AddFriend" :
                if let vc = segue.destinationViewController as? AddFriendController {
                    vc.delegate = self
                }
            default :
                return
            }
        }
    }
    
    // MARK:- UI methods
    @IBAction func addFriends(sender: UIBarButtonItem) {
        performSegueWithIdentifier("AddFriend", sender: nil)
    }
    
    func clickOnButton(button: UIButton) {
        let actionSheetController: UIAlertController = UIAlertController(title: "Changer votre nom", message: nil, preferredStyle: .Alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Annuler", style: .Cancel) { action -> Void in
            //Do nothing
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: "Enregistrer", style: .Default) { action -> Void in
            if let inputText = self.usernameField?.text {
                let newUsername = inputText.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                NSUserDefaults.standardUserDefaults().setObject(newUsername, forKey: "CurrentUserName")
                self.username = newUsername
                self.titleButton?.setTitle(self.username, forState: .Normal)
            }
            
        }
        actionSheetController.addAction(nextAction)
        //Add a text field
        actionSheetController.addTextFieldWithConfigurationHandler { textField -> Void in
            //TextField configuration
            textField.placeholder = "Nouveau nom d'utilisatuer"
            self.usernameField = textField
        }
        
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    func messageDidReceive(sender: NSNotification) {
        let msg = sender.object as NSDictionary
        let from = msg["from"] as String
        let body = msg["body"] as String
        
        var isNewFriend = true
        for i in 0..<friends.count {
            if from == friends[i].mail {
                friends[i].msg = body
                isNewFriend = false
                break
            }
        }
        
        if isNewFriend {
            let friendName = from.componentsSeparatedByString("@")[0]
            User.createInManagedObjectContext(self.managedObjectContext, mail: from, username: from, name: friendName, surname: "", regid: "", type: "")
            appDelegate.saveContext()
            friends.insert((friendName, from, body), atIndex: 0)
        }
    }
    
    func logout() {
        self.refreshControl?.endRefreshing()
        NSUserDefaults.standardUserDefaults().setBool(false, forKey: "PistacheIsLogin")
        NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: "PistacheLastLogoutDate")
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK:- Server
    func getFriends() {
        let recentChats = appDelegate.recentChats
        let fetchRequest = NSFetchRequest(entityName: "User")

        // Create a sort descriptor object that sorts on the "title"
        // property of the Core Data object
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        
        // Set the list of sort descriptors in the fetch request,
        // so it includes the sort descriptor
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let fetchResults = managedObjectContext.executeFetchRequest(fetchRequest, error: nil) as? [User] {
            println(fetchResults.count)
            self.friends=[]
            for i in fetchResults {
                var body = ""
                for chat in recentChats {
                    if chat.mail == i.mail {
                        body = chat.body
                    }
                }
                self.friends += [(i.name+" "+i.surname, i.mail, body)]
            }
            println(self.friends)
        }
//        self.tableView.reloadData()
    }
    
    @IBAction func edit(sender: AnyObject) {
        self.tableView.setEditing(!self.tableView.editing, animated: true)
        if self.tableView.editing {
            let doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: "edit:")
            self.navigationItem.leftBarButtonItem = doneButton
            let logoutButton = UIBarButtonItem(title: "Se déconnecter", style: .Plain, target: self, action: "logout")
            self.navigationItem.rightBarButtonItem = logoutButton
        } else {
            let editButton = UIBarButtonItem(barButtonSystemItem: .Edit, target: self, action: "edit:")
            self.navigationItem.leftBarButtonItem = editButton
            let plusButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "addFriends:")
            self.navigationItem.rightBarButtonItem = plusButton
        }
    }

    // MARK:- Table View Methods
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.friends.count;
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        cell = self.tableView.dequeueReusableCellWithIdentifier("FriendCell") as UITableViewCell
        cell.textLabel?.text = self.friends[indexPath.row].name
        cell.detailTextLabel?.text = self.friends[indexPath.row].msg
        return cell;
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        appDelegate.updateRecentChats(friends[indexPath.row].mail,
            body: friends[indexPath.row].msg)
        self.performSegueWithIdentifier("chat", sender: indexPath)
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch editingStyle {
        case .Delete:
            // remove the deleted item from the model
            let friendToDelete = self.friends[indexPath.row]
            
            let fetchRequest = NSFetchRequest()
            let entity = NSEntityDescription.entityForName("User", inManagedObjectContext: self.managedObjectContext)
            fetchRequest.entity = entity
            // Specify criteria for filtering which objects to fetch
            let predicate = NSPredicate(format: "mail==%@", friendToDelete.mail)
            fetchRequest.predicate = predicate
            
            if let fetchedObjects = self.managedObjectContext.executeFetchRequest(fetchRequest, error: nil)  {
                if fetchedObjects.count > 0 {
                    let objectToDelete = fetchedObjects[0] as User;
                    self.managedObjectContext.deleteObject(objectToDelete)
                    self.appDelegate.saveContext()
                }
            }
            
            // Refresh the table view to indicate that it's deleted
            self.getFriends()
            
            // Tell the table view to animate out that row
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func didAddFriend() {
        getFriends()
        self.tableView.reloadData()
    }
    
    func currentFriends() -> [String] {
        var mails = [String]()
        for (name, mail, msg) in friends {
            mails += [mail]
        }
        
        return mails
    }
    
}
