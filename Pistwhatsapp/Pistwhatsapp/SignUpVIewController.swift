//
//  SignUpVIewController.swift
//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 13/02/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        self.usernameField.delegate = self
        self.emailField.delegate = self
        self.passwordField.delegate = self
    }
    
       
    @IBAction func saveAction(sender: AnyObject) {
        if !self.usernameField.text.isEmpty && !self.emailField.text.isEmpty && !self.passwordField.text.isEmpty {
            send()
        } else {
            let alert = UIAlertView(title: nil, message: "Need username, email and password to sign up", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    @IBAction func CancelAction(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func SendAction(sender: AnyObject) {
        println("ici")
        if !self.usernameField.text.isEmpty && !self.emailField.text.isEmpty && !self.passwordField.text.isEmpty {
            send()
        } else {
            let alert = UIAlertView(title: nil, message: "Need username, email and password to sign up", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    func send() {
        println("signUpbeforesend")
        let request = NSMutableURLRequest(URL: NSURL(string: "https://pist-minesnantes.rhcloud.com/sendtoken.php")!)
        var regid = "regid\(emailField.text)\(usernameField.text)\(passwordField.text)\(NSDate().timeIntervalSince1970)"
        if let token = NSUserDefaults.standardUserDefaults().stringForKey("Token") {
            regid = token
        }
        request.HTTPMethod = "POST"
        let postString = "mail=\(emailField.text)&password=\(passwordField.text)&token=\(regid)&system=ios"
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
            (data, response, error) in
            
            var errorMessage = ""
            if error != nil {
                println("error=\(error)")
                errorMessage = error.localizedDescription
            } else {
                if let httpResponse = response as? NSHTTPURLResponse {
                    println(httpResponse.statusCode)
                    if httpResponse.statusCode==200 {
                        let userDefaults = NSUserDefaults.standardUserDefaults()
                        println("signUpcreateuser")
                        userDefaults.setObject(self.emailField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()),
                            forKey: "CurrentUserEmail")
                        userDefaults.setObject(self.usernameField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()),
                            forKey: "CurrentUserName")
                        userDefaults.setObject(self.passwordField.text, forKey: "CurrentUserPassword")
                        println("Succes")
                        self.dismissViewControllerAnimated(true, completion: nil)
                    } else {
                        println("Fail")
                        errorMessage = "Fail"
                    }
                } else {
                    assertionFailure("unexpected response")
                    errorMessage = "Unexpected response"
                }
                
                println("response = \(response)")
                let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("responseString = \(responseString)")
            }
            
            if !errorMessage.isEmpty {
                dispatch_async(dispatch_get_main_queue()) {
                    let alert = UIAlertView(title: nil, message: errorMessage, delegate: nil, cancelButtonTitle: "OK")
                    alert.show()
                }
            }
        }
        
        task.resume()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.usernameField {
            self.emailField.becomeFirstResponder()
        } else if textField == self.emailField {
            self.passwordField.becomeFirstResponder()
        } else if textField == self.passwordField {
            textField.resignFirstResponder()
            if !self.usernameField.text.isEmpty && !self.emailField.text.isEmpty && !self.passwordField.text.isEmpty {
                self.send()
            } else {
                let alert = UIAlertView(title: nil, message: "Need username, email and password to sign up", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        }
        return true
    }
}
