//
//  ChatViewController.swift
//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 13/02/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import UIKit
import CoreData

class ChatViewController: JSQMessagesViewController {
    
    var friendMail: String?
    var friendName: String?
    var userMail: String?
    
    var messages: [Message] = []
    var chats: [JSQMessage] = []
    var numUnread = 0
    
    lazy var bubbleFactory = JSQMessagesBubbleImageFactory()
    
    lazy var outgoingBubbleColor: UIColor = UIColor.jsq_messageBubbleBlueColor()
    lazy var incomingBubbleColor: UIColor = UIColor.jsq_messageBubbleLightGrayColor()
    
    let pageSize = 20
    var pageNum = 1
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        return (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!
    }()
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as AppDelegate
    }()
    
    @IBOutlet weak var sidebarButton: UIBarButtonItem!
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let mail = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserEmail") {
            userMail = mail
            self.senderId = userMail
            self.senderDisplayName = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserName")
        }
        
        if let mail = NSUserDefaults.standardUserDefaults().stringForKey("CurrentFriendMail") {
            friendMail = mail
            friendName = NSUserDefaults.standardUserDefaults().stringForKey("CurrentFriendName")
            self.title = friendName
        }
        
//        self.showLoadEarlierMessagesHeader = true
        self.showTypingIndicator = true
        self.automaticallyScrollsToMostRecentMessage = true
        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        if let revealViewController = self.revealViewController() {
            self.view.addGestureRecognizer(revealViewController.tapGestureRecognizer())
        }
        
        self.getRecentHistory()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("messageDidReceive:"), name: ChatNotification.MessageDidReceive.rawValue, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("currentFriendMailDidChange:"), name: ChatNotification.CurrentFriendMailDidChange.rawValue, object: nil)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        pageNum = 1
        self.getRecentHistory()
        if numUnread > 0 {
            self.sidebarButton.title = "Liste (\(numUnread))"
        } else {
            self.sidebarButton.title = "Liste"
        }
        self.finishReceivingMessageAnimated(true)
    }
    
    // MARK:- JSQMessagesCollectionViewDataSource
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageData! {
        return self.chats[indexPath.row]
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageBubbleImageDataSource! {
        let chat = self.chats[indexPath.row]
        
        if chat.senderId == self.senderId {
            return bubbleFactory.outgoingMessagesBubbleImageWithColor(self.outgoingBubbleColor)
        } else {
            return bubbleFactory.incomingMessagesBubbleImageWithColor(self.incomingBubbleColor)
        }
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAtIndexPath indexPath: NSIndexPath!) -> JSQMessageAvatarImageDataSource! {
        return JSQMessagesAvatarImageFactory.avatarImageWithImage(UIImage(named: "defaultAvatar")!, diameter: UInt(kJSQMessagesCollectionViewAvatarSizeDefault))
    }
    
    // MARK:- JSQMessagesCollectionViewDelegateFlowLayout
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAtIndexPath indexPath: NSIndexPath!) {
        println("tap bubble in \(indexPath.row)")
    }
    
    override func collectionView(collectionView: JSQMessagesCollectionView!, header headerView: JSQMessagesLoadEarlierHeaderView!, didTapLoadEarlierMessagesButton sender: UIButton!) {
        println("tap \"load earlier header\"")
        ++pageNum
        self.getRecentHistory()
    }
    
    // MARK:- CollectionViewDataSource
    
    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chats.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAtIndexPath: indexPath) as JSQMessagesCollectionViewCell
        if chats[indexPath.row].senderId != self.senderId {
            cell.textView.textColor = UIColor.blackColor()
        } else {
            cell.textView.textColor = UIColor.whiteColor()
        }
        return cell
    }
    
    // MARK:- JSQMessagesViewController Methods Override
    
    override func didPressSendButton(button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: NSDate!) {
        sendMessage(text)
        self.finishSendingMessageAnimated(true)
    }

    // MARK:- Model Methods
    func getRecentHistory() {
        let fetchRequest = NSFetchRequest(entityName: "Message")
        let predicate = NSPredicate(format: "from==%@ || to==%@", friendMail!, friendMail!)
        fetchRequest.predicate = predicate
        
        let count = managedObjectContext.countForFetchRequest(fetchRequest, error: nil)
        
        var chatSize = 0
        if count > pageSize * pageNum {
            self.showLoadEarlierMessagesHeader = true
            fetchRequest.fetchOffset = count - pageSize * pageNum
            fetchRequest.fetchLimit = pageSize * pageNum
        } else {
            self.showLoadEarlierMessagesHeader = false
        }
        
        if let fetchResults = managedObjectContext.executeFetchRequest(fetchRequest, error: nil) as? [Message] {
            self.messages=[]
            self.chats=[]
            for i in fetchResults {
                self.messages.insert(i, atIndex: 0)
            }
        }
        
        for i in self.messages{
            if i.from == userMail && i.to == friendMail {
                self.chats.insert(JSQMessage(senderId: userMail, displayName: self.senderDisplayName, text: i.body), atIndex: 0)
                continue
            }
            
            if i.from == friendMail && i.to == userMail {
                self.chats.insert(JSQMessage(senderId: friendMail, displayName: friendName, text: i.body), atIndex: 0)
                continue
            }
        }
        
        if let last = chats.last {
            if last.senderId == userMail {
                appDelegate.updateRecentChats(friendMail!, body: "vous: \(last.text)")
            } else {
                appDelegate.updateRecentChats(friendMail!, body: last.text)
            }
        }
        self.messages = []
    }
    
    func sendMessage(message: String) {
        let usermail = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserEmail")!
        let username = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserName")!
        println("start message Send from: \(usermail) \(username) \(friendMail!)")
        
        if !message.isEmpty {
            let request = NSMutableURLRequest(URL: NSURL(string:  "https://pist-minesnantes.rhcloud.com/messagehandler.php")!)
            
            request.HTTPMethod = "POST"
            let postString = "mail=\(friendMail!)&from=\(usermail)&msg=\(message)"
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                data, response, error in
                
                var errorMessage = ""
                if error != nil {
                    println("error=\(error)")
                    errorMessage = error.localizedDescription
                } else {
                    if let httpResponse = response as? NSHTTPURLResponse {
                        println(httpResponse.statusCode)
                        if httpResponse.statusCode==200 {
                            println("Succes")
                        } else {
                            println("Fail")
                            errorMessage = "Fail"
                        }
                    } else {
                        assertionFailure("unexpected response")
                        errorMessage = "Unexpected response"
                    }
                    
                    println("response = \(response)")
                    let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
                    println("responseString = \(responseString)")
                }
                if !errorMessage.isEmpty {
                    dispatch_async(dispatch_get_main_queue()) {
                        let alert = UIAlertView(title: nil, message: errorMessage, delegate: nil, cancelButtonTitle: "OK")
                        alert.show()
                    }
                }
            }
            task.resume()
            
            var mes = Message.createInManagedObjectContext(managedObjectContext, to: friendMail!, from: userMail!, body: message)
            appDelegate.saveContext()
            self.messages += [mes]
            self.chats += [JSQMessage(senderId: self.senderId, displayName: self.senderDisplayName, text: message)]
            self.appDelegate.updateRecentChats(friendMail!, body: "vous: \(message)")
        }
    }
    
    func messageDidReceive(sender: NSNotification) {
        if let msg = sender.object as? NSDictionary {
            if let from = msg["from"] as? String {
                if from != friendMail {
                    self.sidebarButton.title = "Liste (\(++numUnread))"
                } else {
                    if let body = msg["body"] as? String {
                        chats += [JSQMessage(senderId: friendMail, displayName: friendName, text: body)]
                        self.finishReceivingMessageAnimated(true)
                    }
                }
            }
        }
    }
    
    func currentFriendMailDidChange(sender: NSNotification) {
        println("currentFriendMailDidChange")
        if let mail = NSUserDefaults.standardUserDefaults().stringForKey("CurrentFriendMail") {
            friendMail = mail
            friendName = NSUserDefaults.standardUserDefaults().stringForKey("CurrentFriendName")
            self.title = friendMail
        }
        self.viewDidAppear(true)
    }
    
    @IBAction func listButtonAction(sender: UIBarButtonItem) {
        numUnread = 0
        self.sidebarButton.title = "Liste"
        self.revealViewController().revealToggleAnimated(true)
    }
}
