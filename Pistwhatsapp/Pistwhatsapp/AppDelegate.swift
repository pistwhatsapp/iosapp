//
//  AppDelegate.swift
//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 13/02/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import UIKit
import CoreData
import Security
import AudioToolbox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var password: String?
    
    var soundId = SystemSoundID(kSystemSoundID_Vibrate)

    // MARK: - Life Cycle
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes: (UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound), categories: nil))
        UIApplication.sharedApplication().registerForRemoteNotifications()
        // Create the sound
        let url = NSURL(string: "/System/Library/Audio/UISounds/SIMToolkitGeneralBeep.caf")
        AudioServicesCreateSystemSoundID(url, &soundId)
        
        if let options = launchOptions {
            if let notifications = options[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary {
                if let msg = notifications["msg"] as? NSDictionary {
                    if let from = msg["from"] as? String {
                        if let to = msg["to"] as? String {
                            if let body = msg["body"] as? String {
                                println("Received A notification: \(body)")
                                // TODO: Save new message to CoreData
                                var mes = Message.createInManagedObjectContext(self.managedObjectContext!, to: to, from: from, body: body)
                                self.saveContext()
                                NSNotificationCenter.defaultCenter().postNotificationName(ChatNotification.MessageDidReceive.rawValue, object: msg)
                            }
                        }
                    }
                }

            }
        }
        
        application.applicationIconBadgeNumber = 0
        application.cancelAllLocalNotifications()
        
        return true
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        var deviecTokenString: NSString = deviceToken.description
        deviecTokenString = deviecTokenString.stringByReplacingOccurrencesOfString("<", withString: "")
        deviecTokenString = deviecTokenString.stringByReplacingOccurrencesOfString(">", withString: "")
        deviecTokenString = deviecTokenString.stringByReplacingOccurrencesOfString(" ", withString: "")
        NSUserDefaults.standardUserDefaults().setObject(deviecTokenString, forKey: "Token")
        println(deviecTokenString)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        if NSUserDefaults.standardUserDefaults().boolForKey("PistacheIsLogin") {
            NSUserDefaults.standardUserDefaults().setObject(NSDate(), forKey: "PistacheLastLogoutDate")
        }
        self.saveContext()
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        if let msg = userInfo["msg"] as? NSDictionary {
            if let from = msg["from"] as? String {
                if let to = msg["to"] as? String {
                    if let body = msg["body"] as? String {
                        println("Received A notification: \(body)")
                        var mes = Message.createInManagedObjectContext(self.managedObjectContext!, to: to, from: from, body: body)
                        self.saveContext()
                        self.updateRecentChats(from, body: body)
                        NSNotificationCenter.defaultCenter().postNotificationName(ChatNotification.MessageDidReceive.rawValue, object: msg)
                        AudioServicesPlayAlertSound(soundId)
                    }
                }
            }
        }
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: NSURL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "fr.emn.Pistwhatsapp" in the application's documents Application Support directory.
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.count-1] as NSURL
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = NSBundle.mainBundle().URLForResource("Pistwhatsapp", withExtension: "momd")!
        return NSManagedObjectModel(contentsOfURL: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator? = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator: NSPersistentStoreCoordinator? = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.URLByAppendingPathComponent("Pistwhatsapp.sqlite")
        var error: NSError? = nil
        var failureReason = "There was an error creating or loading the application's saved data."
        let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
        if coordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: options, error: &error) == nil {
            coordinator = nil
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            dict[NSUnderlyingErrorKey] = error
            error = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(error), \(error!.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext? = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        if coordinator == nil {
            return nil
        }
        var managedObjectContext = NSManagedObjectContext()
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if let moc = self.managedObjectContext {
            var error: NSError? = nil
            if moc.hasChanges && !moc.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog("Unresolved error \(error), \(error!.userInfo)")
                abort()
            }
        }
    }
    
    // MARK:- Utilities
    
    var recentChats: [(mail: String, avatar: String, title: String, body: String)] = []
    
    func updateRecentChats(mail: String, body: String) {
        let title = mail.componentsSeparatedByString("@")[0]
        let avatar = ""
        var newBody = body
        for i in 0..<self.recentChats.count {
            if recentChats[i].mail == mail {
                if newBody.isEmpty {
                    newBody = recentChats[i].body
                }
                recentChats.removeAtIndex(i)
                break
            }
        }
        recentChats.insert((mail, avatar, title, newBody), atIndex: 0)
    }
}

