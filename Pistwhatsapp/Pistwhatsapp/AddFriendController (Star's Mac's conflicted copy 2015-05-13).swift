//
//  AddFriendController.swift
//  Pistwhatsapp
//
//  Created by Thomas Garesse on 03/04/2015.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AddFriendController: UITableViewController {

    // MARK:- Model
    var users: [(email: String, selected: Boolean)] = []
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        return (UIApplication.sharedApplication().delegate as AppDelegate).managedObjectContext!
    }()
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as AppDelegate
    }()
    
    // MARK:- View life cycle
    override func viewDidLoad() {
        getAllUsers()
    }
    
    // MARK:- UI methods
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func comfirmAddFriends(sender: UIBarButtonItem) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("UserCell") as UITableViewCell
        cell.textLabel?.text = "test\(indexPath.row)"
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if cell?.accessoryType == UITableViewCellAccessoryType.Checkmark
    }
    
    // MARK:- HTTP
    private func getAllUsers() {
        if let url = NSURL(string: "https://pist-minesnantes.rhcloud.com/getusers.php") {
            var request = NSMutableURLRequest(URL: url)
            request.HTTPMethod = "POST"
            
            let usermail = NSUserDefaults.standardUserDefaults().stringForKey("CurrentUserEmail")!
            let postString = "user=\(usermail)"
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
            
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {
                (data, response, error) in
                println(NSString(data: data!, encoding: NSUTF8StringEncoding))
                println(response)
                println(error)
                
            }
            
            task.resume()
        }
    }

}
