//
//  SidebarViewController.swift
//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 13/02/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import UIKit

class SidebarViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var recentChats: [(mail: String, avatar: String, title: String, body: String)] = []
    
    lazy var appDelegate: AppDelegate = {
        return UIApplication.sharedApplication().delegate as AppDelegate
        }()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func backToFriendsList(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
//        var frame = self.tableView.frame
//        var newFrame = CGRect(x: frame.origin.x, y: frame.origin.y, width: 250, height: frame.height)
//        self.tableView.frame = newFrame
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        getRecentChats()
        self.tableView.reloadData()
    }
    
    // MARK:- Table View Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1 // we have only one section
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.recentChats.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCellWithIdentifier("ConversationCell") as UITableViewCell
        cell.detailTextLabel?.text = self.recentChats[indexPath.row].body
        cell.textLabel?.text = self.recentChats[indexPath.row].title
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        NSUserDefaults.standardUserDefaults().setObject(self.recentChats[indexPath.row].mail, forKey: "CurrentFriendMail")
        NSNotificationCenter.defaultCenter().postNotificationName(ChatNotification.CurrentFriendMailDidChange.rawValue, object: self.recentChats[indexPath.row].mail)
        self.revealViewController().revealToggle(self)
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        switch editingStyle {
        case .Delete:
            // remove the deleted item from the model
            let chatToDelete = self.recentChats[indexPath.row].mail
            var indexToDelete = -1
            let delegateChats = appDelegate.recentChats
            for i in 0..<delegateChats.count {
                if delegateChats[i].mail == chatToDelete {
                    indexToDelete = i
                    break
                }
            }
            
            if indexToDelete != -1 {
                appDelegate.recentChats.removeAtIndex(indexToDelete)
            }
            
            // Refresh the table view to indicate that it's deleted
            self.getRecentChats()
            
            // Tell the table view to animate out that row
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        default:
            return
        }
    }

    
    func getRecentChats() {
        let currentFriendMail = NSUserDefaults.standardUserDefaults().stringForKey("CurrentFriendMail")
        self.recentChats = appDelegate.recentChats
        
        for i in 0..<appDelegate.recentChats.count {
            if appDelegate.recentChats[i].mail == currentFriendMail {
                self.recentChats.removeAtIndex(i)
                break
            }
        }
    }
}
