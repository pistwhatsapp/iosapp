//
//  Message.swift
//  Pistwhatsapp
//
//  Created by Thomas Garesse on 17/04/2015.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import Foundation
import CoreData

@objc(Message)
class Message: NSManagedObject {
    
    @NSManaged var to: String
    @NSManaged var from: String
    @NSManaged var body: String

    
    
    class func createInManagedObjectContext(moc: NSManagedObjectContext, to: String, from: String, body:String) -> Message {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("Message", inManagedObjectContext: moc) as Message
        newItem.to = to
        newItem.from = from
        newItem.body = body
        
        return newItem
    }
    
}