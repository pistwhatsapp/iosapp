//
//  User.swift
//  Pistwhatsapp
//
//  Created by Xiaxing SHI on 13/03/15.
//  Copyright (c) 2015 Mines Nantes. All rights reserved.
//

import Foundation
import CoreData

@objc(User)
class User: NSManagedObject {

    @NSManaged var mail: String
    @NSManaged var username: String
    @NSManaged var name: String
    @NSManaged var surname: String
    @NSManaged var regid: String
    @NSManaged var type: String
    

    
    class func createInManagedObjectContext(moc: NSManagedObjectContext, mail: String, username: String, name:String, surname:String, regid:String, type:String) -> User {
        let newItem = NSEntityDescription.insertNewObjectForEntityForName("User", inManagedObjectContext: moc) as User
        newItem.mail = mail
        newItem.username = username
        newItem.name = name
        newItem.surname = surname
        newItem.regid = regid
        newItem.type = type
        
        return newItem
    }
    
}
